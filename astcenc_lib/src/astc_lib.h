#ifndef __ASTC_LIB_H__
#define __ASTC_LIB_H__
#include "astc_stdint.h"
#include "astc_codec_internals.h"

namespace astc
{
#ifndef MAX_PATH
#define MAX_PATH 260
#endif

struct astc_header
{
    uint8_t magic[4];
    uint8_t blockdim_x;
    uint8_t blockdim_y;
    uint8_t blockdim_z;
    uint8_t xsize[3];			// x-size = xsize[0] + xsize[1] + xsize[2]
    uint8_t ysize[3];			// x-size, y-size and z-size are given in texels;
    uint8_t zsize[3];			// block count is inferred
};

enum class SimplePixelFormat
{
    R8,
    RG8,
    RGB8,
    RGBA8,
    BGR8,
    BGRA8,
    L8,
    LA8,

    RGBX8,
    BGRX8,

    R16,
    RG16,
    RGB16,
    RGBA16,
    BGR16,
    BGRA16,
    L16,
    LA16,

    R16F,
    RG16F,
    RGB16F,
    RGBA16F,
    BGR16F,
    BGRA16F,
    L16F,
    LA16F,

    R32F,
    RG32F,
    RGB32F,
    RGBA32F,
    BGR32F,
    BGRA32F,
    L32F,
    LA32F,
    INVALID,
};

enum class ConvertAstcToDataPixel
{
    RGBA8,
    RGBA16,
};

enum class AstcQuality
{
    VERYFAST,
    FAST,
    MEDIUM,
    THOROUGH,
    EXHAUSTIVE,
};

class AstcAPI
{
public:
    AstcAPI() = delete;
    ~AstcAPI() = delete;

    static bool Init();
    struct Buffer
    {
        void* data = nullptr;
        uint32_t size = 0;
    };

    //compute covert to astc needed data size
    static uint32_t ComputeConvertAstcSize(uint32_t x, uint32_t y, uint32_t z, uint32_t xdim, uint32_t ydim, uint32_t zdim);

    struct SampleImage
    {
        Buffer buf;
        uint32_t x = 0;
        uint32_t y = 0;
        uint32_t z = 0;
        SimplePixelFormat format = SimplePixelFormat::INVALID;
        void Destory()
        {
            free(buf.data);
            buf.data = nullptr;
            buf.size = 0;
            x = 0;
            y = 0;
            z = 0;
            format = SimplePixelFormat::INVALID;
        }
    };

    struct ConvertAstcParam
    {
    public:
        uint32_t xdim = 4;
        uint32_t ydim = 4;
        uint32_t zdim = 1;
        astc_decode_mode decode_mode = DECODE_HDR;
        AstcQuality quality = AstcQuality::MEDIUM;
        uint32_t thread_count = 0;
    public:
        // this param should not set by user, I don't known this mean
        error_weighting_params ewp;
        swizzlepattern swz_encode;
        swizzlepattern swz_decode;
    public:
        ConvertAstcParam()
        {
            ewp.rgb_power = 1.0f;
            ewp.alpha_power = 1.0f;
            ewp.rgb_base_weight = 1.0f;
            ewp.alpha_base_weight = 1.0f;
            ewp.rgb_mean_weight = 0.0f;
            ewp.rgb_stdev_weight = 0.0f;
            ewp.alpha_mean_weight = 0.0f;
            ewp.alpha_stdev_weight = 0.0f;

            ewp.rgb_mean_and_stdev_mixing = 0.0f;
            ewp.mean_stdev_radius = 0;
            ewp.enable_rgb_scale_with_alpha = 0;
            ewp.alpha_radius = 0;

            ewp.block_artifact_suppression = 0.0f;
            ewp.rgba_weights[0] = 1.0f;
            ewp.rgba_weights[1] = 1.0f;
            ewp.rgba_weights[2] = 1.0f;
            ewp.rgba_weights[3] = 1.0f;
            ewp.ra_normal_angular_scale = 0;

            ewp.partition_search_limit = 25;
            ewp.block_mode_cutoff = 0.75;
            ewp.partition_1_to_2_limit = 1.2;
            ewp.lowest_correlation_cutoff = 0.75;
            ewp.max_refinement_iters = 2;
            ewp.texel_avg_error_limit = 22251.8438;

            swz_encode = { 0, 1, 2, 3 };
            swz_decode = { 0, 1, 2, 3 };
            float log10_texels_2d = log((float)(xdim * ydim)) / log(10.0f);
            float log10_texels_3d = log((float)(xdim * ydim * zdim)) / log(10.0f);

            switch (quality)
            {
            case AstcQuality::VERYFAST:
            {
                ewp.partition_search_limit = 2;
                ewp.partition_1_to_2_limit = 1.0f;
                ewp.lowest_correlation_cutoff = 0.5;

                float dblimit_autoset_2d = MAX(70 - 35 * log10_texels_2d, 53 - 19 * log10_texels_2d);
                float dblimit_autoset_3d = MAX(70 - 35 * log10_texels_3d, 53 - 19 * log10_texels_3d);
                float texel_avg_error_limit_2d = pow(0.1f, dblimit_autoset_2d * 0.1f) * 65535.0f * 65535.0f;
                float texel_avg_error_limit_3d = pow(0.1f, dblimit_autoset_3d * 0.1f) * 65535.0f * 65535.0f;
                ewp.texel_avg_error_limit = ((zdim == 1) ? texel_avg_error_limit_2d: texel_avg_error_limit_3d);

                ewp.block_mode_cutoff = 25 / 100.0f;
                ewp.max_refinement_iters = 1;

            }
                break;
            case AstcQuality::FAST:
            {
                ewp.partition_search_limit = 4;
                ewp.partition_1_to_2_limit = 1.0f;
                ewp.lowest_correlation_cutoff = 0.5;
                float dblimit_autoset_2d = MAX(85 - 35 * log10_texels_2d, 63 - 19 * log10_texels_2d);
                float dblimit_autoset_3d = MAX(85 - 35 * log10_texels_3d, 63 - 19 * log10_texels_3d);
                float texel_avg_error_limit_2d = pow(0.1f, dblimit_autoset_2d * 0.1f) * 65535.0f * 65535.0f;
                float texel_avg_error_limit_3d = pow(0.1f, dblimit_autoset_3d * 0.1f) * 65535.0f * 65535.0f;
                ewp.texel_avg_error_limit = ((zdim == 1) ? texel_avg_error_limit_2d: texel_avg_error_limit_3d);

                ewp.block_mode_cutoff = 50 / 100.0f;
                ewp.max_refinement_iters = 1;
            }
                break;
            case AstcQuality::MEDIUM:
            {
                ewp.partition_search_limit = 25;
                ewp.partition_1_to_2_limit = 1.2f;
                ewp.lowest_correlation_cutoff = 0.75;
                float dblimit_autoset_2d = MAX(95 - 35 * log10_texels_2d, 70 - 19 * log10_texels_2d);
                float dblimit_autoset_3d = MAX(95 - 35 * log10_texels_3d, 70 - 19 * log10_texels_3d);
                float texel_avg_error_limit_2d = pow(0.1f, dblimit_autoset_2d * 0.1f) * 65535.0f * 65535.0f;
                float texel_avg_error_limit_3d = pow(0.1f, dblimit_autoset_3d * 0.1f) * 65535.0f * 65535.0f;
                ewp.texel_avg_error_limit = ((zdim == 1) ? texel_avg_error_limit_2d : texel_avg_error_limit_3d);

                ewp.block_mode_cutoff = 75 / 100.0f;
                ewp.max_refinement_iters = 2;
            }
                break;
            case AstcQuality::THOROUGH:
            {
                ewp.partition_search_limit = 100;
                ewp.partition_1_to_2_limit = 2.5f;
                ewp.lowest_correlation_cutoff = 0.95;
                float dblimit_autoset_2d = MAX(105 - 35 * log10_texels_2d, 77 - 19 * log10_texels_2d);
                float dblimit_autoset_3d = MAX(105 - 35 * log10_texels_3d, 77 - 19 * log10_texels_3d);
                float texel_avg_error_limit_2d = pow(0.1f, dblimit_autoset_2d * 0.1f) * 65535.0f * 65535.0f;
                float texel_avg_error_limit_3d = pow(0.1f, dblimit_autoset_3d * 0.1f) * 65535.0f * 65535.0f;
                ewp.texel_avg_error_limit = ((zdim == 1) ? texel_avg_error_limit_2d : texel_avg_error_limit_3d);

                ewp.block_mode_cutoff = 95 / 100.0f;
                ewp.max_refinement_iters = 4;
            }
                break;
            case AstcQuality::EXHAUSTIVE:
            {
                ewp.partition_search_limit = PARTITION_COUNT;
                ewp.partition_1_to_2_limit = 1000.0f;
                ewp.lowest_correlation_cutoff = 0.99;
                ewp.texel_avg_error_limit = 999.0f;

                ewp.block_mode_cutoff = 1.0f;
                ewp.max_refinement_iters = 4;
            }
                break;
            default:
                break;
            }
            
        }
    };

    static void GenerateAstcHeader(uint32_t x, uint32_t y, uint32_t z, uint32_t xdim, uint32_t ydim, uint32_t zdim, astc_header& outHeader);

    //covert image data to astc data,not has astc header
    static bool ConvertDataToAstc(const SampleImage& info, const ConvertAstcParam & param, Buffer& dstBuffer, uint32_t& channelCount);
    // 需要转换的SampleImage.buf.size(通道）大小需要等到软解压后才能计算出，所以在函数内计算并分配SampleImage.buf的大小及指针
    // 用完后需使用者手动SampleImage::Destroy调用删除其内存
    bool CreateAndConvertAstcToData(const astc_header* srcHeader, const Buffer& srcBuffer, SampleImage& dst, astc_decode_mode decode_mode = DECODE_LDR_SRGB);


    // old main func,以后再改为参数形式
    static bool Convert(int argc, char** argv);
private:
    static void StoreAstcImageToBuffer(const astc_codec_image* input_image, int xdim, int ydim, int zdim, 
        const error_weighting_params* ewp, astc_decode_mode decode_mode, swizzlepattern swz_encode, 
        int threadcount, Buffer& outBuffer);
private:
    struct AstcArgument
    {
        astc_decode_mode decode_mode = DECODE_HDR;
        int opmode = 0;					// 0=compress, 1=decompress, 2=do both, 4=compare
        char input_filename[MAX_PATH];
        char output_filename[MAX_PATH];
        int xdim = 4;
        int ydim = 4;
        int zdim = 1;

        bool silentmode = false;
        bool timemode = false;
        bool psnrmode = false;
        error_weighting_params ewp;
        swizzlepattern swz_encode;
        swizzlepattern swz_decode;


        //just use fill
        int thread_count = 0;		// default value
        int thread_count_autodetected = 0;

        int preset_has_been_set = 0;

        int plimit_autoset = -1;
        int plimit_user_specified = -1;
        int plimit_set_by_user = 0;

        float dblimit_autoset_2d = 0.0;
        float dblimit_autoset_3d = 0.0;
        float dblimit_user_specified = 0.0;
        int dblimit_set_by_user = 0;

        float oplimit_autoset = 0.0;
        float oplimit_user_specified = 0.0;
        int oplimit_set_by_user = 0;

        float mincorrel_autoset = 0.0;
        float mincorrel_user_specified = 0.0;
        int mincorrel_set_by_user = 0;

        float bmc_user_specified = 0.0;
        float bmc_autoset = 0.0;
        int bmc_set_by_user = 0;

        int maxiters_user_specified = 0;
        int maxiters_autoset = 0;
        int maxiters_set_by_user = 0;

        int pcdiv = 1;

        int target_bitrate_set = 0;
        float target_bitrate = 0;

        int print_block_mode_histogram = 0;

        float log10_texels_2d = 0.0f;
        float log10_texels_3d = 0.0f;

        int low_fstop = -10;
        int high_fstop = 10;

        int array_size = 1;



        AstcArgument()
        {
            ewp.rgb_power = 1.0f;
            ewp.alpha_power = 1.0f;
            ewp.rgb_base_weight = 1.0f;
            ewp.alpha_base_weight = 1.0f;
            ewp.rgb_mean_weight = 0.0f;
            ewp.rgb_stdev_weight = 0.0f;
            ewp.alpha_mean_weight = 0.0f;
            ewp.alpha_stdev_weight = 0.0f;

            ewp.rgb_mean_and_stdev_mixing = 0.0f;
            ewp.mean_stdev_radius = 0;
            ewp.enable_rgb_scale_with_alpha = 0;
            ewp.alpha_radius = 0;

            ewp.block_artifact_suppression = 0.0f;
            ewp.rgba_weights[0] = 1.0f;
            ewp.rgba_weights[1] = 1.0f;
            ewp.rgba_weights[2] = 1.0f;
            ewp.rgba_weights[3] = 1.0f;
            ewp.ra_normal_angular_scale = 0;

            swz_encode = { 0, 1, 2, 3 };
            swz_decode = { 0, 1, 2, 3 };
        }
    };
private:
    static void PrintHelp();

    static bool ParseArgument(int argc, char** argv, AstcArgument& outArg);
};


} //end namespace

#endif // 
