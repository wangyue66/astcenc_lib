#include "astc_log.h"
#include <stdarg.h>
#include <stdio.h>
#ifdef WIN32
#	define WIN32_LEAN_AND_MEAN
#	define _CRT_SECURE_NO_WARNINGS
#	include <Windows.h>
#endif

namespace astc
{
void PrintInfo(char* msg, ...)
{
    using namespace std;
    // toString
    static char szBuffer[8192];
    va_list args;
    va_start(args, msg);
    vsprintf(szBuffer, msg, args);
    va_end(args);

    printf("%s", szBuffer);
    fflush(stdout);
#ifdef WIN32
    OutputDebugStringA(szBuffer);
#endif


}

} //end namespace
