#ifndef __ASTC_LOG_H__
#define __ASTC_LOG_H__

namespace astc
{
extern void PrintInfo(char* msg, ...);
}

#define ASTC_LOGV(formats, ...) astc::PrintInfo(formats, ##__VA_ARGS__);
#endif
