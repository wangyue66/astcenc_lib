#include "astc_codec_internals.h"
#include "astc_log.h"

int g_block_mode_histogram[2048];
int g_perform_srgb_transform = 0;
int g_rgb_force_use_of_hdr = 0;
int g_alpha_force_use_of_hdr = 0;


#ifdef DEBUG_PRINT_DIAGNOSTICS
int g_print_diagnostics = 0;
int g_diagnostics_tile = -1;
#endif

int g_print_tile_errors = 0;

int g_print_statistics = 0;

int g_progress_counter_divider = 1;


void astc_codec_internal_error(const char *filename, int linenum)
{
    ASTC_LOGV("Internal error: File=%s Line=%d\n", filename, linenum);
    return;
}

#ifndef WIN32
double get_time()
{
    timeval tv;
    gettimeofday(&tv, 0);

    return (double)tv.tv_sec + (double)tv.tv_usec * 1.0e-6;
}


int astc_codec_unlink(const char *filename)
{
    return unlink(filename);
}

#else


int pthread_create(pthread_t * thread, const pthread_attr_t * attribs, void *(*threadfunc) (void *), void *thread_arg)
{
    *thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)threadfunc, thread_arg, 0, NULL);
    return 0;
}

int pthread_join(pthread_t thread, void **value)
{
    WaitForSingleObject(thread, INFINITE);
    return 0;
}

double get_time()
{
    FILETIME tv;
    GetSystemTimeAsFileTime(&tv);

    unsigned __int64 ticks = tv.dwHighDateTime;
    ticks = (ticks << 32) | tv.dwLowDateTime;

    return ((double)ticks) / 1.0e7;
}

// Define an unlink() function in terms of the Win32 DeleteFile function.
int astc_codec_unlink(const char *filename)
{
    BOOL res = DeleteFileA(filename);
    return (res ? 0 : -1);
}
#endif

